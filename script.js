"use strict";



// Экранирование это символ \ который обычно является началом спецсимвола, который в свою очередь может перенести строку либо сделать отступ и несколько других функций.
//Function declaration, Function Expression.
//Перемещает все объявления в верхнюю часть текущей области видимости.






function createNewUser  () {
    const firstName = prompt("Enter name please.");
    const lastName = prompt("Enter last name please.");
    const birthday = prompt("Enter birthday date please (Format dd.mm.yyyy)");
    const newUser = {
        firstName,
        lastName,
        birthday,
        getPassword() {
            return (firstName[0].toUpperCase() +lastName.toLowerCase() + birthday.slice(birthday.length - 4));
           },
        getAge(){
         let date = Number(birthday.substr(0, 2));
         let month = Number(birthday.substr(3, 5)- 1);
         let year = Number(birthday.substr(6, 10));
         let today = new Date();
         let age = today.getFullYear() - year;
         if (today.getMonth() < month || today.getMonth() === month && today.getDate()< date) {
            age--;
         }
          return age;
        },
        
    };
    return newUser;
};
const newUser = createNewUser();
console.log(newUser.getPassword());
console.log(newUser.getAge());


